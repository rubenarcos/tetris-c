All content is licensed under a Creative Commons Attribution 4.0 International License
[![LiLicensee: license.markdownC BY-NC-ND 4.0](https://licensebuttons.net/l/by-nc-nd/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/)
 
# Tetris C#
Ejercicio de demostración
 
## Descripción
Aplicación, tipo escritorio en C# para la demostración de la utilización de este. 
 
## Documentación 
Información disponible en: https://www.rarcos.com/c-tetris/

### Licencia
[License about details](https://bitbucket.org/rubenarcos/tetris-c/src/master/license.md)