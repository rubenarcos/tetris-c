﻿namespace Tetris
{
    partial class frmPuntuaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPuntuaciones));
            this.dataGridPuntuaciones = new System.Windows.Forms.DataGridView();
            this.Iniciales = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Puntos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPuntuaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridPuntuaciones
            // 
            this.dataGridPuntuaciones.AllowUserToOrderColumns = true;
            this.dataGridPuntuaciones.AllowUserToResizeColumns = false;
            this.dataGridPuntuaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPuntuaciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Iniciales,
            this.Puntos});
            this.dataGridPuntuaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridPuntuaciones.Location = new System.Drawing.Point(0, 0);
            this.dataGridPuntuaciones.Name = "dataGridPuntuaciones";
            this.dataGridPuntuaciones.ReadOnly = true;
            this.dataGridPuntuaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridPuntuaciones.Size = new System.Drawing.Size(243, 261);
            this.dataGridPuntuaciones.TabIndex = 0;
            this.dataGridPuntuaciones.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridPuntuaciones_CellContentClick);
            // 
            // Iniciales
            // 
            this.Iniciales.HeaderText = "Iniciales";
            this.Iniciales.Name = "Iniciales";
            this.Iniciales.ReadOnly = true;
            // 
            // Puntos
            // 
            this.Puntos.HeaderText = "Puntos";
            this.Puntos.Name = "Puntos";
            this.Puntos.ReadOnly = true;
            // 
            // frmPuntuaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 261);
            this.Controls.Add(this.dataGridPuntuaciones);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPuntuaciones";
            this.Text = "Puntuaciones";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPuntuaciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridPuntuaciones;
        private System.Windows.Forms.DataGridViewTextBoxColumn Iniciales;
        private System.Windows.Forms.DataGridViewTextBoxColumn Puntos;
    }
}