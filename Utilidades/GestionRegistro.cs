﻿using System;
using Microsoft.Win32;
using System.IO.Ports;

namespace Tetris
{
    public class GestionRegistro
    {
        private static string rutaClave = null;
        private static RegistryKey registroApp = null;

        ///<summary>
        ///Asigna la ruta de la clave para las operaciones posteriores.
        ///Debe de ser invocada en primer lugar.
        ///</summary>
        ///<remarks>
        ///Se recomienda que la ruta preceda @ para detección de \ rutas.
        ///La ruta será almacenada para su uso mediante posteriores métodos.
        ///</remarks>
        public GestionRegistro(String RutaClave)
        {
            rutaClave = RutaClave;
        }

        ///<summary>
        ///Comprueba si existe una clave en el registro.
        ///</summary>
        ///<remarks>
        ///Se recomienda que la ruta preceda @ para detección de \ rutas.
        ///Devueve el resultado de la búsqueda de la clave en el registro.
        ///</remarks>
        public bool comprobarExistenciaConfig()
        {
            bool existeRegistro = false;
            try
            {
                registroApp = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(rutaClave, true);
                //Apertura del registro modo escritura
                if (registroApp != null)
                {
                    Console.WriteLine("El registro de la aplicación ya existe.");
                    existeRegistro = true;
                    //registroApp.Close();
                    registroApp = null;
                }
                else
                {
                    if (rutaClave.Contains("Puntuacion"))
                    {
                        registroApp = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(rutaClave);
                        registroApp.SetValue("numPuntuaciones", "0");
                    }
                    else
                    {
                        registroApp = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(rutaClave);
                        registroApp.SetValue("X", "0");
                        registroApp.SetValue("Y", "0");
                    }

                }

            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("El registro de la aplicación no existe. Se creará y guardarán los datos de la configuración que se indique. " + e);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("Ha habido un error al recibir la ruta de la clave." + e);
            }
            return existeRegistro;
        }

        public bool grabarDatosRegistro(string[] datosRegistro)
        {
            bool realizado = true;
            try
            {
                if (rutaClave.Contains("Puntuacion"))
                {
                    Console.WriteLine("PUNTUACION");
                    registroApp = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(rutaClave);
                    int numPuntuaciones = int.Parse(registroApp.GetValue("numPuntuaciones").ToString());
                    registroApp.SetValue("numPuntuaciones", ++numPuntuaciones);
                    registroApp.SetValue(numPuntuaciones.ToString(), datosRegistro[0]);
                    registroApp = null;
                }
                else
                {
                    Console.WriteLine("VENTANA");
                    registroApp = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(rutaClave);
                    registroApp.SetValue("X", datosRegistro[0]);
                    registroApp.SetValue("Y", datosRegistro[1]);
                    registroApp = null;
                }
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("ERROR en la creación del registro: " + ex);
                realizado = false;
            }
            return realizado;
        }

        public string[] cargarDatosRegistro()
        {
            string[] datos = null;
            try
            {
                if (rutaClave.Contains("Puntuacion"))
                {
                    Console.WriteLine("PUNTUACION");
                    registroApp = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(rutaClave, false);
                    int numPuntuaciones = int.Parse(registroApp.GetValue("numPuntuaciones").ToString());
                    datos = new string[numPuntuaciones];
                    for (int i = 0; i < numPuntuaciones; i++)
                    {
                        datos[i] = registroApp.GetValue((i + 1).ToString()).ToString();
                    }
                    registroApp.Close();
                    registroApp = null;
                }
                else
                {
                    Console.WriteLine("VENTANA");
                    registroApp = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(rutaClave, false);
                    datos = new string[] { registroApp.GetValue("X").ToString(), registroApp.GetValue("Y").ToString() };
                    registroApp.Close();
                    registroApp = null;
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("ERROR en la lectura del regitro, " + e);
            }
            return datos;
        }

    }
}
