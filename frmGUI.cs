using System;
using System.Drawing;
using System.Windows.Forms;
using System.Media;
using System.IO;
using System.Collections.Generic;

namespace Tetris
{
    /// <summary>
    /// Descripci�n breve de Form1.
    /// </summary>
    public class frmGUI : System.Windows.Forms.Form
    {
        #region Atributos

        private System.Windows.Forms.PictureBox pbPantallaJuego;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.IContainer components;
        private Tetris t;
        private bool haciaAbajo;
        private bool haciaDerecha;
        private bool rotaDerecha;
        private bool rotaIzquierda;
        private bool enPausa, juegoIniciado;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lblNumLineas;
        private System.Windows.Forms.Label lblNivel;
        private System.Windows.Forms.Label lblPartidaNivel;
        private System.Windows.Forms.Label lblPartidaLinea;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuHerramientasJuego;
        private System.Windows.Forms.MenuItem menuHerramientasAyuda;
        private System.Windows.Forms.MenuItem menuJuegoNuevo;
        private System.Windows.Forms.MenuItem menuJuegoSalir;
        private System.Windows.Forms.MenuItem menuAyudaAcercaDe;
        private System.Windows.Forms.Label lblPartidaProxima;
        private System.Windows.Forms.PictureBox pbPiezaSiguiente;
        private System.Windows.Forms.MenuItem menuJuegoPausa;
        private ToolTip mensajeInformativo;
        private MenuItem menuHerramientasOpciones;
        private MenuItem menuOpcionesAumentarFilas;
        private MenuItem menuOpcionesAumentarColumnas;
        private MenuItem menuItem9;
        private MenuItem menuOpcionesReducirFilas;
        private MenuItem menuOpcionesReducirColumnas;
        private Label lblPausado;
        private bool haciaIzquierda;
        private MenuItem menuAyudaAcercaDe2;
        private NotifyIcon iconoNotificacion;
        private ContextMenuStrip menuIconoNotificacion;
        private ToolStripMenuItem menuNotificacionAcercaDe;
        private ToolStripSeparator separadorMenuNotificacionSalir;
        private ToolStripMenuItem menuNotificacionSalir;
        private Label lblPartidaPuntuacion;
        private Label lblPuntuacion;
        private SoundPlayer musica;
        private MenuItem menuJuegoPuntuaciones;
        private TextBox txtIniciales;
        private Label lblIniciales;
        private Button btnJugar;
        private List<string[]> listaPuntuaciones;
        private string iniciales;
        private GestionRegistro registroPuntuaciones;
        private GestionRegistro registroPosVentana;

        #endregion

        public frmGUI()
        {
            //
            // Necesario para admitir el Dise�ador de Windows Forms
            //
            InitializeComponent();
            musicaTetris();
            registroPosVentana = new GestionRegistro(@"SOFTWARE\RubenArcos\Tetris\Ventana");
            comprobarPosVentana();
            registroPosVentana = null;
            iniciales = null;
            //
            // TODO: agregar c�digo de constructor despu�s de llamar a InitializeComponent
            //
        }

        #region C�digo generado por el Dise�ador de Windows Forms
        /// <summary>
        /// M�todo necesario para admitir el Dise�ador. No se puede modificar
        /// el contenido del m�todo con el editor de c�digo.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGUI));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lblNumLineas = new System.Windows.Forms.Label();
            this.lblNivel = new System.Windows.Forms.Label();
            this.lblPartidaNivel = new System.Windows.Forms.Label();
            this.lblPartidaLinea = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuHerramientasJuego = new System.Windows.Forms.MenuItem();
            this.menuJuegoNuevo = new System.Windows.Forms.MenuItem();
            this.menuJuegoPausa = new System.Windows.Forms.MenuItem();
            this.menuJuegoPuntuaciones = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuJuegoSalir = new System.Windows.Forms.MenuItem();
            this.menuHerramientasOpciones = new System.Windows.Forms.MenuItem();
            this.menuOpcionesAumentarFilas = new System.Windows.Forms.MenuItem();
            this.menuOpcionesReducirFilas = new System.Windows.Forms.MenuItem();
            this.menuOpcionesAumentarColumnas = new System.Windows.Forms.MenuItem();
            this.menuOpcionesReducirColumnas = new System.Windows.Forms.MenuItem();
            this.menuHerramientasAyuda = new System.Windows.Forms.MenuItem();
            this.menuAyudaAcercaDe = new System.Windows.Forms.MenuItem();
            this.menuAyudaAcercaDe2 = new System.Windows.Forms.MenuItem();
            this.lblPartidaProxima = new System.Windows.Forms.Label();
            this.mensajeInformativo = new System.Windows.Forms.ToolTip(this.components);
            this.pbPiezaSiguiente = new System.Windows.Forms.PictureBox();
            this.lblPuntuacion = new System.Windows.Forms.Label();
            this.lblPausado = new System.Windows.Forms.Label();
            this.pbPantallaJuego = new System.Windows.Forms.PictureBox();
            this.iconoNotificacion = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuIconoNotificacion = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuNotificacionAcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.separadorMenuNotificacionSalir = new System.Windows.Forms.ToolStripSeparator();
            this.menuNotificacionSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.lblPartidaPuntuacion = new System.Windows.Forms.Label();
            this.txtIniciales = new System.Windows.Forms.TextBox();
            this.lblIniciales = new System.Windows.Forms.Label();
            this.btnJugar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbPiezaSiguiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPantallaJuego)).BeginInit();
            this.menuIconoNotificacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 800;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lblNumLineas
            // 
            this.lblNumLineas.BackColor = System.Drawing.Color.Transparent;
            this.lblNumLineas.ForeColor = System.Drawing.Color.Black;
            this.lblNumLineas.Location = new System.Drawing.Point(16, 152);
            this.lblNumLineas.Name = "lblNumLineas";
            this.lblNumLineas.Size = new System.Drawing.Size(80, 23);
            this.lblNumLineas.TabIndex = 1;
            this.lblNumLineas.Text = "0";
            this.lblNumLineas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mensajeInformativo.SetToolTip(this.lblNumLineas, "N�mero de l�neas necesarias para subir de nivel");
            // 
            // lblNivel
            // 
            this.lblNivel.BackColor = System.Drawing.Color.Transparent;
            this.lblNivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNivel.ForeColor = System.Drawing.Color.Black;
            this.lblNivel.Location = new System.Drawing.Point(16, 208);
            this.lblNivel.Name = "lblNivel";
            this.lblNivel.Size = new System.Drawing.Size(80, 23);
            this.lblNivel.TabIndex = 2;
            this.lblNivel.Text = "1";
            this.lblNivel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mensajeInformativo.SetToolTip(this.lblNivel, "Nivel en curso");
            // 
            // lblPartidaNivel
            // 
            this.lblPartidaNivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartidaNivel.Location = new System.Drawing.Point(16, 192);
            this.lblPartidaNivel.Name = "lblPartidaNivel";
            this.lblPartidaNivel.Size = new System.Drawing.Size(40, 16);
            this.lblPartidaNivel.TabIndex = 4;
            this.lblPartidaNivel.Text = "Nivel:";
            // 
            // lblPartidaLinea
            // 
            this.lblPartidaLinea.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartidaLinea.Location = new System.Drawing.Point(16, 136);
            this.lblPartidaLinea.Name = "lblPartidaLinea";
            this.lblPartidaLinea.Size = new System.Drawing.Size(40, 16);
            this.lblPartidaLinea.TabIndex = 5;
            this.lblPartidaLinea.Text = "Lineas:";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuHerramientasJuego,
            this.menuHerramientasOpciones,
            this.menuHerramientasAyuda});
            // 
            // menuHerramientasJuego
            // 
            this.menuHerramientasJuego.Index = 0;
            this.menuHerramientasJuego.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuJuegoNuevo,
            this.menuJuegoPausa,
            this.menuJuegoPuntuaciones,
            this.menuItem9,
            this.menuJuegoSalir});
            this.menuHerramientasJuego.Text = "Juego";
            // 
            // menuJuegoNuevo
            // 
            this.menuJuegoNuevo.Index = 0;
            this.menuJuegoNuevo.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.menuJuegoNuevo.Text = "Nuevo";
            this.menuJuegoNuevo.Click += new System.EventHandler(this.btnJugar_Click);
            // 
            // menuJuegoPausa
            // 
            this.menuJuegoPausa.Enabled = false;
            this.menuJuegoPausa.Index = 1;
            this.menuJuegoPausa.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.menuJuegoPausa.Text = "Pausa";
            this.menuJuegoPausa.Click += new System.EventHandler(this.menuItem6_Click);
            // 
            // menuJuegoPuntuaciones
            // 
            this.menuJuegoPuntuaciones.Index = 2;
            this.menuJuegoPuntuaciones.Text = "Puntuaciones";
            this.menuJuegoPuntuaciones.Click += new System.EventHandler(this.menuJuegoPuntuaciones_Click);
            // 
            // menuItem9
            // 
            this.menuItem9.Index = 3;
            this.menuItem9.Text = "-";
            // 
            // menuJuegoSalir
            // 
            this.menuJuegoSalir.Index = 4;
            this.menuJuegoSalir.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.menuJuegoSalir.Text = "Salir";
            this.menuJuegoSalir.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuHerramientasOpciones
            // 
            this.menuHerramientasOpciones.Index = 1;
            this.menuHerramientasOpciones.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuOpcionesAumentarFilas,
            this.menuOpcionesReducirFilas,
            this.menuOpcionesAumentarColumnas,
            this.menuOpcionesReducirColumnas});
            this.menuHerramientasOpciones.Text = "Opciones";
            // 
            // menuOpcionesAumentarFilas
            // 
            this.menuOpcionesAumentarFilas.Index = 0;
            this.menuOpcionesAumentarFilas.Text = "Aumentar filas";
            this.menuOpcionesAumentarFilas.Click += new System.EventHandler(this.menuOpcionesAumentarFilas_Click);
            // 
            // menuOpcionesReducirFilas
            // 
            this.menuOpcionesReducirFilas.Index = 1;
            this.menuOpcionesReducirFilas.Text = "Reducir filas";
            this.menuOpcionesReducirFilas.Click += new System.EventHandler(this.menuOpcionesReducirFilas_Click);
            // 
            // menuOpcionesAumentarColumnas
            // 
            this.menuOpcionesAumentarColumnas.Index = 2;
            this.menuOpcionesAumentarColumnas.Text = "Aumentar columnas";
            this.menuOpcionesAumentarColumnas.Click += new System.EventHandler(this.menuOpcionesAumentarColumnas_Click);
            // 
            // menuOpcionesReducirColumnas
            // 
            this.menuOpcionesReducirColumnas.Index = 3;
            this.menuOpcionesReducirColumnas.Text = "Reducir columnas";
            this.menuOpcionesReducirColumnas.Click += new System.EventHandler(this.menuOpcionesReducirColumnas_Click);
            // 
            // menuHerramientasAyuda
            // 
            this.menuHerramientasAyuda.Index = 2;
            this.menuHerramientasAyuda.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuAyudaAcercaDe,
            this.menuAyudaAcercaDe2});
            this.menuHerramientasAyuda.Text = "?";
            // 
            // menuAyudaAcercaDe
            // 
            this.menuAyudaAcercaDe.Index = 0;
            this.menuAyudaAcercaDe.Text = "Acerca de...";
            this.menuAyudaAcercaDe.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // menuAyudaAcercaDe2
            // 
            this.menuAyudaAcercaDe2.Index = 1;
            this.menuAyudaAcercaDe2.Text = "Acerca de, modificaci�n...";
            this.menuAyudaAcercaDe2.Click += new System.EventHandler(this.menuAyudaAcercaDe2_Click);
            // 
            // lblPartidaProxima
            // 
            this.lblPartidaProxima.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartidaProxima.Location = new System.Drawing.Point(16, 24);
            this.lblPartidaProxima.Name = "lblPartidaProxima";
            this.lblPartidaProxima.Size = new System.Drawing.Size(88, 16);
            this.lblPartidaProxima.TabIndex = 8;
            this.lblPartidaProxima.Text = "Pr�xima pieza:";
            // 
            // pbPiezaSiguiente
            // 
            this.pbPiezaSiguiente.BackColor = System.Drawing.Color.Black;
            this.pbPiezaSiguiente.Location = new System.Drawing.Point(16, 40);
            this.pbPiezaSiguiente.Name = "pbPiezaSiguiente";
            this.pbPiezaSiguiente.Size = new System.Drawing.Size(80, 80);
            this.pbPiezaSiguiente.TabIndex = 7;
            this.pbPiezaSiguiente.TabStop = false;
            this.mensajeInformativo.SetToolTip(this.pbPiezaSiguiente, "Siguiente pieza generada");
            this.pbPiezaSiguiente.Click += new System.EventHandler(this.pbPiezaSiguiente_Click);
            // 
            // lblPuntuacion
            // 
            this.lblPuntuacion.BackColor = System.Drawing.Color.Transparent;
            this.lblPuntuacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPuntuacion.ForeColor = System.Drawing.Color.Black;
            this.lblPuntuacion.Location = new System.Drawing.Point(16, 261);
            this.lblPuntuacion.Name = "lblPuntuacion";
            this.lblPuntuacion.Size = new System.Drawing.Size(80, 23);
            this.lblPuntuacion.TabIndex = 11;
            this.lblPuntuacion.Text = "1";
            this.lblPuntuacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mensajeInformativo.SetToolTip(this.lblPuntuacion, "Puntuaci�n actual");
            // 
            // lblPausado
            // 
            this.lblPausado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPausado.AutoSize = true;
            this.lblPausado.BackColor = System.Drawing.Color.Black;
            this.lblPausado.Font = new System.Drawing.Font("Orator Std", 20F, System.Drawing.FontStyle.Bold);
            this.lblPausado.ForeColor = System.Drawing.Color.Red;
            this.lblPausado.Location = new System.Drawing.Point(148, 192);
            this.lblPausado.Name = "lblPausado";
            this.lblPausado.Size = new System.Drawing.Size(236, 36);
            this.lblPausado.TabIndex = 9;
            this.lblPausado.Text = "JUEGO PAUSADO";
            this.lblPausado.Visible = false;
            // 
            // pbPantallaJuego
            // 
            this.pbPantallaJuego.BackColor = System.Drawing.Color.Black;
            this.pbPantallaJuego.Location = new System.Drawing.Point(136, 24);
            this.pbPantallaJuego.Margin = new System.Windows.Forms.Padding(20);
            this.pbPantallaJuego.Name = "pbPantallaJuego";
            this.pbPantallaJuego.Size = new System.Drawing.Size(260, 420);
            this.pbPantallaJuego.TabIndex = 0;
            this.pbPantallaJuego.TabStop = false;
            // 
            // iconoNotificacion
            // 
            this.iconoNotificacion.BalloonTipText = "Pr�ctica Especial - Rub�n Arcos";
            this.iconoNotificacion.ContextMenuStrip = this.menuIconoNotificacion;
            this.iconoNotificacion.Icon = ((System.Drawing.Icon)(resources.GetObject("iconoNotificacion.Icon")));
            this.iconoNotificacion.Text = "Pr�ctica Especial - Rub�n Arcos";
            this.iconoNotificacion.Visible = true;
            this.iconoNotificacion.DoubleClick += new System.EventHandler(this.mostrarVentana);
            // 
            // menuIconoNotificacion
            // 
            this.menuIconoNotificacion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNotificacionAcercaDe,
            this.separadorMenuNotificacionSalir,
            this.menuNotificacionSalir});
            this.menuIconoNotificacion.Name = "menuIconoNotificacion";
            this.menuIconoNotificacion.Size = new System.Drawing.Size(127, 54);
            // 
            // menuNotificacionAcercaDe
            // 
            this.menuNotificacionAcercaDe.Name = "menuNotificacionAcercaDe";
            this.menuNotificacionAcercaDe.Size = new System.Drawing.Size(126, 22);
            this.menuNotificacionAcercaDe.Text = "&Acerca de";
            this.menuNotificacionAcercaDe.Click += new System.EventHandler(this.menuAyudaAcercaDe2_Click);
            // 
            // separadorMenuNotificacionSalir
            // 
            this.separadorMenuNotificacionSalir.Name = "separadorMenuNotificacionSalir";
            this.separadorMenuNotificacionSalir.Size = new System.Drawing.Size(123, 6);
            // 
            // menuNotificacionSalir
            // 
            this.menuNotificacionSalir.Name = "menuNotificacionSalir";
            this.menuNotificacionSalir.Size = new System.Drawing.Size(126, 22);
            this.menuNotificacionSalir.Text = "&Salir";
            this.menuNotificacionSalir.Click += new System.EventHandler(this.menuNotificacionSalir_Click);
            // 
            // lblPartidaPuntuacion
            // 
            this.lblPartidaPuntuacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartidaPuntuacion.Location = new System.Drawing.Point(16, 245);
            this.lblPartidaPuntuacion.Name = "lblPartidaPuntuacion";
            this.lblPartidaPuntuacion.Size = new System.Drawing.Size(80, 16);
            this.lblPartidaPuntuacion.TabIndex = 12;
            this.lblPartidaPuntuacion.Text = "Puntuaci�n:";
            // 
            // txtIniciales
            // 
            this.txtIniciales.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIniciales.Location = new System.Drawing.Point(218, 100);
            this.txtIniciales.MaxLength = 3;
            this.txtIniciales.Name = "txtIniciales";
            this.txtIniciales.Size = new System.Drawing.Size(119, 20);
            this.txtIniciales.TabIndex = 13;
            this.txtIniciales.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtIniciales.Visible = false;
            // 
            // lblIniciales
            // 
            this.lblIniciales.AutoSize = true;
            this.lblIniciales.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblIniciales.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblIniciales.Location = new System.Drawing.Point(218, 81);
            this.lblIniciales.Name = "lblIniciales";
            this.lblIniciales.Size = new System.Drawing.Size(119, 13);
            this.lblIniciales.TabIndex = 14;
            this.lblIniciales.Text = "Introduzca sus iniciales:";
            this.lblIniciales.Visible = false;
            // 
            // btnJugar
            // 
            this.btnJugar.Location = new System.Drawing.Point(218, 128);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(119, 23);
            this.btnJugar.TabIndex = 15;
            this.btnJugar.Text = "Jugar";
            this.btnJugar.UseVisualStyleBackColor = true;
            this.btnJugar.Visible = false;
            this.btnJugar.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // frmGUI
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(416, 461);
            this.Controls.Add(this.btnJugar);
            this.Controls.Add(this.lblIniciales);
            this.Controls.Add(this.txtIniciales);
            this.Controls.Add(this.lblPartidaPuntuacion);
            this.Controls.Add(this.lblPuntuacion);
            this.Controls.Add(this.lblPausado);
            this.Controls.Add(this.lblPartidaProxima);
            this.Controls.Add(this.pbPiezaSiguiente);
            this.Controls.Add(this.lblPartidaLinea);
            this.Controls.Add(this.lblPartidaNivel);
            this.Controls.Add(this.lblNivel);
            this.Controls.Add(this.lblNumLineas);
            this.Controls.Add(this.pbPantallaJuego);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "frmGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Pr�ctica Especial - eTetris";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.cerrarFormulario);
            this.Load += new System.EventHandler(this.frmGUI_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmGUI_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmGUI_KeyUp);
            this.Resize += new System.EventHandler(this.cambiaTamano);
            ((System.ComponentModel.ISupportInitialize)(this.pbPiezaSiguiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPantallaJuego)).EndInit();
            this.menuIconoNotificacion.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void frmGUI_Load(object sender, System.EventArgs e)
        {
            t = new Tetris();
            lblNumLineas.Text = lblNumLineas.Text + " / " + Constantes.NUM_LINEAS_POR_NIVEL;
        }

        #region Controles - Movimientos

        private void frmGUI_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            string strKeyPress = null;
            strKeyPress = e.KeyCode.ToString();
            if (!t.juegoTerminado)
            {
                switch (strKeyPress.ToUpper())
                {
                    case "LEFT":
                        haciaIzquierda = true;
                        //t.muevePiezaIzquierda();
                        //pintaPantalla(t.matrizPantalla);
                        break;
                    case "RIGHT":
                        haciaDerecha = true;
                        //t.muevePiezaDerecha();
                        //pintaPantalla(t.matrizPantalla);
                        break;
                    case "UP":
                        rotaDerecha = true;
                        //t.rotaPiezaDerecha();
                        //pintaPantalla(t.matrizPantalla);
                        break;
                    case "A":
                        rotaIzquierda = true;
                        //t.rotaPiezaIzquierda();
                        //pintaPantalla(t.matrizPantalla);
                        break;
                    case "DOWN":
                        haciaAbajo = true;
                        //t.muevePiezaAbajo();
                        //pintaPantalla(t.matrizPantalla);
                        break;
                    case "P":
                        pausaJuego();
                        break;
                    default:
                        //MessageBox.Show(strKeyPress.ToUpper());
                        break;
                }
            }
            else
            {
                switch (strKeyPress.ToUpper())
                {
                    case "ENTER":
                        break;
                    default:
                        break;
                }
            }
        }

        private void frmGUI_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            string strKeyPress = null;
            strKeyPress = e.KeyCode.ToString();
            if (!t.juegoTerminado)
            {
                switch (strKeyPress.ToUpper())
                {
                    case "LEFT":
                        haciaIzquierda = false;
                        break;
                    case "RIGHT":
                        haciaDerecha = false;
                        break;
                    case "DOWN":
                        haciaAbajo = false;
                        break;
                    case "UP":
                        rotaDerecha = false;
                        break;
                    case "A":
                        rotaIzquierda = false;
                        break;
                    default:
                        //MessageBox.Show(strKeyPress.ToUpper());
                        break;
                }
            }
        }

        private void actualizaPantalla()
        {
            if (haciaAbajo)
            {
                t.muevePiezaAbajo();
            }
            if (haciaDerecha)
            {
                t.muevePiezaDerecha();
            }
            if (haciaIzquierda)
            {
                t.muevePiezaIzquierda();
            }
            if (rotaDerecha)
            {
                t.rotaPiezaDerecha();
                rotaDerecha = false;
            }
            if (rotaIzquierda)
            {
                t.rotaPiezaIzquierda();
                rotaIzquierda = false;
            }
            pintaPantalla(t.matrizPantalla);
        }

        private void inicializaTeclas()
        {
            haciaDerecha = false;
            haciaIzquierda = false;
            rotaDerecha = false;
            rotaIzquierda = false;
            haciaAbajo = false;
            enPausa = false;
        }

        #endregion

        #region Dibujo piezas-pantalla

        public void pintaPantalla(int[,] matrizPantalla)
        {
            Bitmap B = new Bitmap(pbPantallaJuego.Width, pbPantallaJuego.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //Bitmap B = new Bitmap(pbPiezaSiguiente.Width, pbPiezaSiguiente.Height, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            Graphics G = Graphics.FromImage(B);
            G.Clear(Color.Black);
            for (int x = 0; x < Constantes.COLUMNAS_PANTALLA + MatrizPantalla.aumColumnasPantalla; x++)
            {
                for (int y = 0; y < Constantes.FILAS_PANTALLA + MatrizPantalla.aumFilasPantalla; y++)
                {
                    int elemento = matrizPantalla[y, x];
                    if (elemento != 0)
                    {
                        dibujaCuadro(G, y, x, Constantes.COLORES(elemento - 1));
                    }
                }
            }
            pbPantallaJuego.Image = B;
        }

        public void pintaPiezaSiguiente(Pieza p)
        {
            Bitmap B = new Bitmap(pbPiezaSiguiente.Width, pbPiezaSiguiente.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //Bitmap B = new Bitmap(pbPiezaSiguiente.Width, pbPiezaSiguiente.Height, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            Graphics G = Graphics.FromImage(B);
            G.Clear(Color.Black);
            for (int x = 0; x < Constantes.COLUMNAS_PIEZAS; x++)
            {
                for (int y = 0; y < Constantes.FILAS_PIEZAS; y++)
                {
                    int elemento = t.piezaSiguiente[y, x];
                    if (elemento != 0)
                    {
                        dibujaCuadro(G, y, x, Constantes.COLORES(t.piezaSiguiente.color - 1));
                    }
                }
            }
            pbPiezaSiguiente.Image = B;
        }

        private void dibujaCuadro(Graphics G, int Y, int X, Color C)
        {
            int x = (X * Constantes.ANCHO_CELDA) + 1;
            int y = (Y * Constantes.ALTO_CELDA) + 1;
            SolidBrush Br = new SolidBrush(C);
            G.FillRectangle(Br, x, y, Constantes.ANCHO_CELDA - 2, Constantes.ALTO_CELDA - 2);
        }

        #endregion

        #region L�gica juego

        private void timer1_Tick(object sender, System.EventArgs e)
        {
            if (t.numLineas < Constantes.NUM_LINEAS_POR_NIVEL)
            {
                lblNumLineas.Text = t.numLineas.ToString() + " / " + Constantes.NUM_LINEAS_POR_NIVEL;
                lblPuntuacion.Text = t.puntuacion.ToString();
                if (haciaAbajo)
                    actualizaPantalla();
                else
                {
                    haciaAbajo = true;
                    actualizaPantalla();
                    haciaAbajo = false;
                }
                pintaPiezaSiguiente(t.piezaSiguiente);
                if (t.juegoTerminado)
                {
                    timer1.Stop();
                    MessageBox.Show("Se termin� el juego", "Fin partida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void timer2_Tick(object sender, System.EventArgs e)
        {
            if (t.numLineas < Constantes.NUM_LINEAS_POR_NIVEL)
            {
                actualizaPantalla();
            }
            else
            {
                timer2.Stop();
                timer1.Stop();
                lblNumLineas.Text = t.numLineas.ToString() + " / " + Constantes.NUM_LINEAS_POR_NIVEL;
                lblPuntuacion.Text = t.puntuacion.ToString();
                inicializaTeclas();
                t.nuevoNivel();
                MessageBox.Show("��Enhorabuena!! Has pasado al nivel " + (t.nivel + 1), "Nuevo nivel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblNivel.Text = ((int)t.nivel + 1).ToString();
                timer1.Interval = Constantes.NIVELES(t.nivel);
                timer2.Start();
                timer1.Start();
                if (iniciales != null)
                    registroPuntuaciones.grabarDatosRegistro(new string[] { iniciales + "-" + t.puntuacion });
            }
            if (t.juegoTerminado)
            {
                timer2.Stop();
                menuJuegoPausa.Enabled = false;
                menuJuegoPuntuaciones.Enabled = true;
                juegoIniciado = false;
                if (iniciales != null)
                {
                    registroPuntuaciones.grabarDatosRegistro(new string[] { iniciales + "-" + t.puntuacion });
                    registroPuntuaciones = null;
                }
                for (int i = 0; i < 3; i++)
                {
                    Console.Beep();
                }
                musica.Stop();
                musica.Dispose();
            }
        }

        private void nuevoJuego()
        {
            habilitarIniciales(false);
            registroPuntuaciones = new GestionRegistro(@"SOFTWARE\RubenArcos\Tetris\Puntuacion");
            comprobarPuntuaciones();
            inicializaTeclas();
            t.nuevoJuego();
            timer1.Interval = Constantes.NIVELES(t.nivel);

            menuOpcionesAumentarColumnas.Enabled = false;
            menuOpcionesReducirColumnas.Enabled = false;
            menuOpcionesAumentarFilas.Enabled = false;
            menuOpcionesReducirFilas.Enabled = false;
            menuJuegoPausa.Enabled = true;
            menuJuegoPuntuaciones.Enabled = false;

            pintaPantalla(t.matrizPantalla);
            timer1.Start();
            timer2.Start();
            juegoIniciado = true;
            musica.PlayLooping();
            menuJuegoPausa.Text = "Pausa";
        }

        private void pausaJuego()
        {
            if (juegoIniciado)
            {
                if (!enPausa)
                {
                    musica.Stop();
                    Console.Beep();
                    lblPausado.Visible = true;
                    menuJuegoPuntuaciones.Enabled = true;
                    menuJuegoPausa.Text = "Reanudar partida";
                    timer1.Stop();
                    timer2.Stop();
                }
                else
                {
                    musica.PlayLooping();
                    Console.Beep();
                    lblPausado.Visible = false;
                    menuJuegoPuntuaciones.Enabled = false;
                    menuJuegoPausa.Text = "Pausa";
                    timer1.Start();
                    timer2.Start();
                }
                enPausa = !enPausa;
            }
        }

        /// <summary>
        /// Limpiar los recursos que se est�n utilizando.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void musicaTetris()
        {
            try
            {
                musica = new SoundPlayer();
                musica.SoundLocation = System.IO.Directory.GetCurrentDirectory() + @"\musica.wav";
                musica.LoadAsync();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion

        #region Men�s y eventos

        private void menuItem4_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void menuItem3_Click(object sender, System.EventArgs e)
        {
            if (txtIniciales.Text.Length < 3 || txtIniciales.Text == String.Empty || txtIniciales.Text == "   ")
            {
                MessageBox.Show("Debe introducir sus iniciales, para almacenar la puntuaci�n, antes de comenzar.", "Iniciales sin completar", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                iniciales = txtIniciales.Text;
                nuevoJuego();
            }
        }

        private void menuItem5_Click(object sender, System.EventArgs e)
        {
            if (juegoIniciado)
            {
                enPausa = false;
                pausaJuego();
                AcerdaDe a = new AcerdaDe();
                a.ShowDialog();
                a.Dispose();
                enPausa = true;
                pausaJuego();
            }
            else
            {
                AcerdaDe a = new AcerdaDe();
                a.ShowDialog();
                a.Dispose();
            }

        }

        private void menuItem6_Click(object sender, System.EventArgs e)
        {
            pausaJuego();
        }

        private void pbPiezaSiguiente_Click(object sender, EventArgs e)
        {
            //Console.WriteLine("PIEZA SIGUIENTE");
        }

        private void menuOpcionesAumentarFilas_Click(object sender, EventArgs e)
        {
            MatrizPantalla.aumFilasPantalla++;
            refrescarMatrizJuego();
        }

        private void menuOpcionesAumentarColumnas_Click(object sender, EventArgs e)
        {
            MatrizPantalla.aumColumnasPantalla++;
            refrescarMatrizJuego();
        }

        private void menuOpcionesReducirColumnas_Click(object sender, EventArgs e)
        {
            MatrizPantalla.aumColumnasPantalla--;
            refrescarMatrizJuego();
        }

        private void menuOpcionesReducirFilas_Click(object sender, EventArgs e)
        {
            MatrizPantalla.aumFilasPantalla--;
            refrescarMatrizJuego();
        }

        private void cambiaTamano(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized && !enPausa)
            {
                pausaJuego();
            }
        }

        private void menuAyudaAcercaDe2_Click(object sender, EventArgs e)
        {
            if (juegoIniciado)
            {
                enPausa = false;
                pausaJuego();
                frmAcercaDe a = new frmAcercaDe();
                a.ShowDialog();
                a.Dispose();
                enPausa = true;
                pausaJuego();
            }
            else
            {
                frmAcercaDe a = new frmAcercaDe();
                a.ShowDialog();
                a.Dispose();
            }
        }

        private void refrescarMatrizJuego()
        {
            pbPantallaJuego.Height = Constantes.ALTO_CELDA * (Constantes.FILAS_PANTALLA + MatrizPantalla.aumFilasPantalla);
            pbPantallaJuego.Width = Constantes.ANCHO_CELDA * (Constantes.COLUMNAS_PANTALLA + MatrizPantalla.aumColumnasPantalla);
            this.Refresh();
        }

        private void habilitarIniciales(bool estado)
        {
            lblPausado.Visible = estado;
            lblIniciales.Visible = estado;
            txtIniciales.Visible = estado;
            btnJugar.Visible = estado;
            lblIniciales.Enabled = estado;
            txtIniciales.Enabled = estado;
            btnJugar.Enabled = estado;
            txtIniciales.Focus();
        }

        private void mostrarVentana(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void menuNotificacionSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cerrarFormulario(object sender, FormClosingEventArgs e)
        {
            if (juegoIniciado)
            {
                enPausa = false;
                pausaJuego();
            }

            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult dialogo = MessageBox.Show("�Desea Salir de la Aplicacion?", "Salir de Aplicacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (dialogo != DialogResult.OK)
                {
                    e.Cancel = true;
                    enPausa = true;
                    pausaJuego();
                }
                else
                {
                    if (iniciales != null)
                    {
                        try
                        {
                            registroPuntuaciones.grabarDatosRegistro(new string[] { iniciales + "-" + t.puntuacion });
                            registroPuntuaciones = null;
                        }
                        catch (NullReferenceException ex)
                        {
                            Console.WriteLine("Error en la grabaci�n de la puntuaci�n en el registro: " + ex);
                        }

                    }
                    registroPosVentana = new GestionRegistro(@"SOFTWARE\RubenArcos\Tetris\Ventana");
                    registroPosVentana.grabarDatosRegistro(new string[] { this.Bounds.X.ToString(), this.Bounds.Y.ToString() });
                    Console.WriteLine("Almacenada posici�n ventana: " + this.Bounds.X.ToString() + ", " + this.Bounds.Y.ToString());
                }
            }
        }

        private void menuJuegoPuntuaciones_Click(object sender, EventArgs e)
        {
            if (registroPuntuaciones == null)
            {
                registroPuntuaciones = new GestionRegistro(@"SOFTWARE\RubenArcos\Tetris\Puntuacion");
            }
            comprobarPuntuaciones();
            frmPuntuaciones ventanaPuntuaciones = new frmPuntuaciones(listaPuntuaciones);
            ventanaPuntuaciones.ShowDialog();
            registroPuntuaciones = null;
        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            if (juegoIniciado)
            {
                pausaJuego();

                DialogResult dialogo = MessageBox.Show("Hay una partida en curso. �Desea iniciar otra partida nueva?\nSe perder� la puntuaci�n actual.", "Partida nueva", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (dialogo != DialogResult.OK)
                {
                    pausaJuego();
                }
                else
                {
                    habilitarIniciales(true);
                }
            }
            else
            {
                habilitarIniciales(true);
            }
        }

        #endregion

        #region Registro

        private void comprobarPuntuaciones()
        {
            try
            {
                if (registroPuntuaciones.comprobarExistenciaConfig())
                {
                    String[] datosConfig = registroPuntuaciones.cargarDatosRegistro();
                    if (datosConfig != null)
                    {
                        listaPuntuaciones = new List<string[]>();
                        foreach (var i in datosConfig)
                        {
                            listaPuntuaciones.Add(new string[] { i.Substring(0, 3), i.Substring(4, (i.Length - 4)) });
                        }

                        Console.WriteLine("  [PUNTUACIONES]  ");
                        Console.WriteLine("Siglas | Puntos");
                        Console.WriteLine("---------------");
                        foreach (var item in listaPuntuaciones)
                        {
                            Console.WriteLine(item[0] + " | " + item[1]);
                        }
                    }
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("ERROR en el acceso al registro " + e);
            }
        }

        private void comprobarPosVentana()
        {
            try
            {
                if (registroPosVentana.comprobarExistenciaConfig())
                {
                    string[] datosConfig = registroPosVentana.cargarDatosRegistro();
                    if (datosConfig != null)
                    {
                        this.SetBounds(int.Parse(datosConfig[0]), int.Parse(datosConfig[1]), this.Width, this.Height);
                    }
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("ERROR en el acceso al registro " + e);
            }

        }

        #endregion
    }
}
